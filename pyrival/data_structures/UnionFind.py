# {{{ UnionFind
class UnionFind:
    def __init__(self, n):
        self.parent = list(range(n))
        self.n = n
        self._size = [1] * n
        self.num_groups = n

    def find(self, a):
        acopy = a
        while a != self.parent[a]:
            a = self.parent[a]
        while acopy != a:
            self.parent[acopy], acopy = a, self.parent[acopy]
        return a

    def union(self, a, b):
        a, b = self.find(a), self.find(b)
        if a == b:
            return

        if self._size[a] < self._size[b]:
            a, b = b, a

        self.num_groups -= 1
        self.parent[b] = a
        self._size[a] += self._size[b]

    def same(self, a, b):
        return self.find(a) == self.find(b)

    def size(self, a):
        return self._size[self.find(a)]

    def groups(self):
        gs = dict()
        for u in range(self.n):
            p = self.find(u)
            if p not in gs:
                gs[p] = []
            gs[p].append(u)

        return list(gs.values())

    def __len__(self):
        return self.num_groups


# }}}
